FROM java:8

RUN cd /usr/bin && curl --location "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar zx

ENTRYPOINT []
CMD []